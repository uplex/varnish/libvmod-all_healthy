/*-
 * Copyright 2018 UPLEX Nils Goroll Systemoptimierung
 *
 * All rights reserved.
 *
 * Author: Nils Goroll <nils.goroll@uplex.de>
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "config.h"

#include <string.h>	// memset for INIT_OBJ
#include <stdlib.h>	// free/calloc

#include <cache/cache.h>
#include <vcl.h>
#include <vtim.h>

#include "vcc_all_healthy_if.h"

struct vmod_all_healthy_director {
	unsigned				magic;
#define VMOD_ALL_HEALTHY_DIRECTOR_MAGIC	0x0d8790a3
	VCL_BACKEND				dir;
	VCL_BACKEND				backend;
	int					spcconsider;
	int					nconsider;
	VCL_BACKEND				*consider;
};

static VCL_BACKEND vmod_director_resolve(VRT_CTX, VCL_BACKEND b)
{
	struct vmod_all_healthy_director *d;

	CAST_OBJ_NOTNULL(d, b->priv, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);

	return (d->backend);
}

/*
 * we can only approximate the change time for the unhealthy case because we do
 * not have the full record: consider all backends healthy, a goes down, then b,
 * a comes back up: we would need to record the time a went down initially. We
 * approximate by returning the time b went down, so:
 *
 * - unhealthy: earliest change of unhealthy backend
 * - healthy: latest change of healthy backend
 */

static VCL_BOOL
vmod_director_healthy(VRT_CTX, VCL_BACKEND b, VCL_TIME *t)
{
	struct vmod_all_healthy_director *d;
	int i;
	VCL_BOOL br, r = 1;
	VCL_TIME bt, tt[2];
	VCL_BACKEND be;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CAST_OBJ_NOTNULL(d, b->priv, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);

	if (t) {
		tt[0] = VTIM_real();
		tt[1] = 0.0;
	}

	for (i = 0; i < d->nconsider; i++) {
		be = d->consider[i];
		CHECK_OBJ_NOTNULL(be, DIRECTOR_MAGIC);
		br = !!VRT_Healthy(ctx, be, &bt);
		r &= br;
		if (t) {
			if (r && br) {
				if (bt > tt[1])
					tt[1] = bt;
			} else if (! br) {
				if (bt < tt[0])
					tt[0] = bt;
			}
		}
	}

	assert(r == 0 || r == 1);
	if (t)
		*t = tt[r];

	return (r);
}

static void v_matchproto_(vdi_release_f)
vmod_director_release(VCL_BACKEND dir)
{
	struct vmod_all_healthy_director *d;
	int i;

	CHECK_OBJ_NOTNULL(dir, DIRECTOR_MAGIC);
	CAST_OBJ_NOTNULL(d, dir->priv, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);

	for (i = 0; i < d->nconsider; i++)
		VRT_Assign_Backend(&d->consider[i], NULL);
	VRT_Assign_Backend(&d->backend, NULL);
}

static void v_matchproto_(vdi_destroy_f)
vmod_director_destroy(VCL_BACKEND dir)
{
	struct vmod_all_healthy_director *d;

	CHECK_OBJ_NOTNULL(dir, DIRECTOR_MAGIC);
	CAST_OBJ_NOTNULL(d, dir->priv, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);

	free(TRUST_ME(d->consider));
	FREE_OBJ(d);
}

static const struct vdi_methods vmod_director_methods[1] = {{
	.magic =	VDI_METHODS_MAGIC,
	.type =		"all_healthy",
	.healthy =	vmod_director_healthy,
	.resolve =	vmod_director_resolve,
	.release =	vmod_director_release,
	.destroy =	vmod_director_destroy,
}};

VCL_VOID
vmod_director__init(VRT_CTX,
    struct vmod_all_healthy_director **dp, const char *vcl_name)
{
	struct vmod_all_healthy_director *d;
	const int spc = 4;

	AN(dp);
	AZ(*dp);
	ALLOC_OBJ(d, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);
	if (d == NULL) {
		VRT_fail(ctx, "obj alloc failed");
		return;
	}
	d->consider = calloc(spc, sizeof(VCL_BACKEND));
	if (d->consider == NULL) {
		VRT_fail(ctx, "consider list alloc failed");
		goto fail_consider;
	}
	d->spcconsider = spc;
	d->dir = VRT_AddDirector(ctx, vmod_director_methods, d, "%s", vcl_name);
	if (d->dir == NULL) {
		VRT_fail(ctx, "AddDirector failed");
		goto fail_dir;
	}
	*dp = d;
	return;

  fail_dir:
	free(TRUST_ME(d->consider));
  fail_consider:
	FREE_OBJ(d);
	return;
}

VCL_VOID
vmod_director__fini(struct vmod_all_healthy_director **dp)
{
	struct vmod_all_healthy_director *d = *dp;
	int i;

	*dp = NULL;
	if (d == NULL)
		return;
	CHECK_OBJ(d, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);
	VRT_DelDirector(&d->dir);
}

#define check_init(ctx, name) do {					\
		if (((ctx)->method & VCL_MET_INIT) == 0) {		\
			VRT_fail(ctx, "." #name				\
			    " can only be called from vcl_init {}");	\
			return;						\
		}							\
	} while(0)

VCL_VOID
vmod_director_consider(VRT_CTX,
    struct vmod_all_healthy_director *d, VCL_BACKEND b)
{
	int i;
	void *n;

	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ(d, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);
	check_init(ctx, consider);

	if (b == NULL) {
		VRT_fail(ctx, "cannot consider a NULL backend");
		return;
	}
	for (i = 0; i < d->nconsider; i++) {
		if (d->consider[i] == b)
			return;
	}
	if (d->spcconsider == d->nconsider) {
		i = d->spcconsider << 1;
		n = realloc(d->consider, i * sizeof(VCL_BACKEND));
		if (n == NULL) {
			VRT_fail(ctx, "growing consider list failed");
			return;
		}
		d->spcconsider = i;
		d->consider = n;
	}
	VRT_Assign_Backend(&d->consider[d->nconsider++], b);
	if (d->backend == NULL)
		VRT_Assign_Backend(&d->backend, b);
}

VCL_VOID
vmod_director_set_backend(VRT_CTX,
    struct vmod_all_healthy_director *d, VCL_BACKEND b)
{
	CHECK_OBJ_NOTNULL(ctx, VRT_CTX_MAGIC);
	CHECK_OBJ(d, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);
	check_init(ctx, consider);

	if (b == NULL) {
		VRT_fail(ctx, "cannot set a NULL backend");
		return;
	}

	VRT_Assign_Backend(&d->backend, b);
}

VCL_BACKEND
vmod_director_backend(VRT_CTX, struct vmod_all_healthy_director *d)
{
	CHECK_OBJ(d, VMOD_ALL_HEALTHY_DIRECTOR_MAGIC);

	(void) ctx;

	return (d->dir);
}
