..
.. NB:  This file is machine generated, DO NOT EDIT!
..
.. Edit vmod.vcc and run make instead
..

.. role:: ref(emphasis)

================
VMOD all_healthy
================

--------------------------------------------------
Director which is only healthy if all backends are
--------------------------------------------------

:Manual section: 3

SYNOPSIS
========

.. parsed-literal::

  import all_healthy [as name] [from "path"]
  
  new xdirector = all_healthy.director()
  
      VOID xdirector.consider(BACKEND)
   
      VOID xdirector.set_backend(BACKEND)
   
      BACKEND xdirector.backend()
   
DESCRIPTION
===========

The all_healthy vmod provides a varnish director tieing the health
state of several other backends / health states: An all_healthy
director is only healthy if all backends to consider are healthy. It
always resolves to exactly one backend, which may or may not be part
of the set of backends considered for health state.

An all_healthy director will commonly be layered below other directors
for actual load balancing.

Examples and use cases:

* Using more than one health checks for a backend::

	probe probe_a { ... }
	probe probe_b { ... }

	backend be_a {
		.host = "1.2.3.4";
		.probe = probe_a;
	}

	# will never actually be used, only provides the second probe
	backend be_b {
		.host = "1.2.3.4";
		.probe = probe_b;
	}

	sub vcl_init {
		new be = all_healthy.director();
		# implies be.set_backend(be_a);
		be.consider(be_a);
		be.consider(be_b);

		some_director.add_backend(be);
	}

	sub vcl_backend_fetch {
		set bereq.backend = be.backend();
	}

* Checking health on a different port::

	probe oob_probe { ... }

	backend be_traffic {
		.host = "1.2.3.4";
		# no .probe !
	}

	backend be_oob_probe {
		.host = "1.2.3.4";
		.port = "4242";
		.probe = oob_probe;
	}

	sub vcl_init {
		new be = all_healthy.director();
		be.consider(be_oob_probe);
		be.set_backend(be_traffic);

		some_director.add_backend(be);
	}

	sub vcl_backend_fetch {
		set bereq.backend = be.backend();
	}

.. _all_healthy.director():

new xdirector = all_healthy.director()
--------------------------------------

Instantiate an all_healthy director.

.. _xdirector.consider():

VOID xdirector.consider(BACKEND)
--------------------------------

Add a backend to consider for determining the health state of the
director.

This method may only be called from vcl_init {}

.. _xdirector.set_backend():

VOID xdirector.set_backend(BACKEND)
-----------------------------------

Set the backend the director resolves to.

This method may only be called from vcl_init {}

.. _xdirector.backend():

BACKEND xdirector.backend()
---------------------------

Return the all_healthy director instance, which will resolve to the
backend set using the .set_backend() method.

SEE ALSO
========vcl\(7),varnishd\(1)

COPYRIGHT
=========

::

  This document is copyright and licensed under the same conditions as
  the libvmod-all_healthy project. See LICENSE for details.
